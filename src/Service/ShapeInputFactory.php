<?php

namespace Service;

class ShapeInputFactory
{
    public static function make()
    {
        $type = $_POST['type'];
        $existingShapeTypes = array_unique($type);

        $shapeSquare = [];
        $shapeCircle = [];

        foreach ($existingShapeTypes as $type) {
            switch ($type) {
                case 'Square';
                    $shapeSquare = self::makeInputSquareArray($type);
                    break;

                case 'Circle';
                    $shapeCircle = self::makeInputCircleArray($type);
                    break;
            }

        }

        $shapeInput = array_merge($shapeSquare, $shapeCircle);

        return $shapeInput;
    }

    /**
     * create the input parameter based on the shape
     * @param string $type
     * @return array
     */
    private static function makeInputSquareArray(string $type)
    {
        $sx1 = $_POST['sx1'];
        $sx2 = $_POST['sx2'];
        $sy1 = $_POST['sy1'];
        $sy2 = $_POST['sy2'];
        $sColor = $_POST['sColor'];
        $sBorderSize = $_POST['sBorderSize'];

        $shapeInputArray = [];
        for ($counter = 0; $counter < count($sx1); $counter++) {

            $shapeInputArray[] = ['type' => $type,
                'params' => [
                    $sx1[$counter],
                    $sy1[$counter],
                    $sx2[$counter],
                    $sy2[$counter],
                    $sColor[$counter],
                    $sBorderSize[$counter]
                ]
            ];
        }

        return $shapeInputArray;
    }

    /**
     * create the input parameter based on the shape
     * @param string $type
     * @return array
     */
    private static function makeInputCircleArray(string $type)
    {
        $cx = $_POST['cx'];
        $cy = $_POST['cy'];
        $width = $_POST['cWidth'];
        $height = $_POST['cHeight'];
        $cColor = $_POST['cColor'];
        $cBorderSize = $_POST['cBorderSize'];

        $shapeInputArray = [];
        for ($counter = 0; $counter < count($cx); $counter++) {

            $shapeInputArray[] = ['type' => $type,
                'params' => [
                    $cx[$counter],
                    $cy[$counter],
                    $width[$counter],
                    $height[$counter],
                    $cColor[$counter],
                    $cBorderSize[$counter]
                ]
            ];
        }

        return $shapeInputArray;
    }
}
