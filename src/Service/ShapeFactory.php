<?php

namespace Service;

class ShapeFactory
{
    public static function create(string $shapeType, array $params, $img)
    {
        $black = imagecolorallocate($img, 0, 0, 0);

        switch ($shapeType) {
            case 'Square';
                imagesetthickness($img, $params[5]);
                list($r, $g, $b) = sscanf($params[4], "#%02x%02x%02x");
                $color = imagecolorallocate($img, $r, $g, $b);
                imagefilledrectangle($img, $params[0], $params[1], $params[2], $params[3], $color);
                imagerectangle($img, $params[0], $params[1], $params[2], $params[3], $black);
                break;

            case 'Circle';
                imagesetthickness($img, $params[5]);
                list($r, $g, $b) = sscanf($params[4], "#%02x%02x%02x");
                $color = imagecolorallocate($img, $r, $g, $b);
                imagefilledellipse($img, $params[0], $params[1], $params[2], $params[3], $color);
                imageellipse($img, $params[0], $params[1], $params[2], $params[3], $black);
                break;
        }
    }
}
