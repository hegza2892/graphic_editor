<?php

include 'autoload.php';

use Controller\ShapeController;
use Controller\ViewController;

$url = $_SERVER['REQUEST_URI'];

if ($url == '/') {
    $controller = new ViewController();
    echo $controller->show();

} elseif ($url == '/draw') {
    $controller = new ShapeController();
     $controller->draw();
}