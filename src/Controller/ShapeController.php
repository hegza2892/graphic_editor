<?php

namespace Controller;

use Service\ShapeFactory;
use Service\ShapeInputFactory;

class ShapeController
{
    public function draw()
    {
        $shapeInput = ShapeInputFactory::make();

        $imgWidth = 2000;
        $imgHeight = 2000;
        $img = imageCreate($imgWidth, $imgHeight);
        $white = imagecolorallocate($img, 255, 255, 255);
        imagefill($img, 0, 0, $white);

        foreach ($shapeInput as $key => $value) {
            $type = array_values($value)[0];
            $params = array_values($value)[1];

            ShapeFactory::create($type, $params, $img);
        }

        header('Content-Type: image/png');
        imagepng($img);
        imagedestroy($img);
    }
}