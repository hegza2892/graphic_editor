<?php

namespace Controller;

class ViewController
{
    public function show()
    {
        $path = realpath(dirname(__FILE__) . '/../View/view.html');

        return file_get_contents($path);
    }
}