# Graphic Editor

The implementation is done using php 7.2. 

# Technology

- PHP 7.2
- Docker

# Running the application

- run `docker-compose up -d` to bring the application container up and running.

# Available endpoints

| Name            |  Method  |    URL                        | 
| ------          | -----    |   ------                      | 
| /               | `GET`   | `http://localhost:8000/`       | 
                                                                                     
## Endpoint

- A `GET` request `http://localhost:8000/` get shape parameters by form. After submitting form, the shape(s) will be displayed
 on the separate page(/draw) with the mime type image/png. 
